package sample;

import sample.objects.Obiekt;

import java.util.List;
import java.util.Map;

public class KomendaRobota {


    int kodOperacji;
    String argumenty[];
    String argumenty_zakodowane[];
    int liczbaArgumentow;
    int typ;


    public KomendaRobota(int kodOperacji, int liczbaArgumentow) {
        this.kodOperacji = kodOperacji;
        this.liczbaArgumentow = liczbaArgumentow;
        argumenty = new String[liczbaArgumentow];
        argumenty_zakodowane = new String[liczbaArgumentow];
        typ = 1; //komenda wykonawcza
    }

    public int getTyp() {
        return typ;
    }

    public void setTyp(int typ) {
        this.typ = typ;
    }



    public String getArg(int index) {
        //rozkoduj(pam);
        if (index < liczbaArgumentow) {
            return argumenty[index];
        } else {
            return null;
        }
    }

    public int getArgInt(int index) {


        try {
            return Integer.parseInt(argumenty[index]);
        } catch (NumberFormatException ex) {
            System.out.println("Nie moze byc liczba");
        } catch (IndexOutOfBoundsException ex) {
            System.out.println("Nie istnieje argument o tym indeksie");
        }
        return -1;
    }


    public void rozkoduj(PamiecZmiennych pam,List<Obiekt> ElementyGry){

       for(int i=0;i<liczbaArgumentow;i++){

           if (argumenty_zakodowane[i].contains("$"))
           argumenty[i]=pam.ciagZPodmienionymiZmiennymi(argumenty_zakodowane[i]);

           if (argumenty_zakodowane[i].contains("#"))
           argumenty[i]=pam.ciagZPodmienionymiStalymi(argumenty_zakodowane[i],ElementyGry);

       }

       //rozkoduj stale



    }





    public void setArg(int index, String wartosc) {
        if (index < liczbaArgumentow) {
            argumenty[index] = wartosc;
            argumenty_zakodowane[index] = wartosc;
        } else {
            System.out.println("Nie istnieje argument o tym indeksie");
        }
    }


    public void p() {
        System.out.print(kodOperacji);
        for (String arg : argumenty) {
            System.out.print(" " + arg);
        }
        System.out.println();
    }

    public void p2() {
        System.out.print(kodOperacji);
        for (String arg : argumenty_zakodowane) {
            System.out.print(" " + arg);
        }
        System.out.println();
    }

    public int getKodOperacji() {
        return kodOperacji;
    }







}
