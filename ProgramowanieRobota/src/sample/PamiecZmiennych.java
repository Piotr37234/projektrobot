package sample;

import sample.objects.Obiekt;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PamiecZmiennych {

    public Map<String, Integer> zmienne = new HashMap<>();

    public Map<String, Integer> getZmienne() {
        return zmienne;
    }

    public void clearZmienne() {
        zmienne.clear();
    }

    public String ciagZPodmienionymiZmiennymi(String text) {

        for (String key : zmienne.keySet()) {
            // System.out.println("key: "+key);
            if (text.contains("$" + key)) {
                // System.out.println("key: "+key);
                text = text.replace("$" + key, getZmienna(key) + "");
            }

        }
        return text;
    }

    public String ciagZPodmienionymiStalymi(String text,List<Obiekt> ElementyGry) {
        String tmp = text.replace("#","");


        Obiekt o = wezObiekt(tmp,ElementyGry);

        if(o!=null){
            return tmp;
        }
        else{
            return text;
        }

    }


    public Obiekt wezObiekt(String nazwa,List<Obiekt> ElementyGry) {
        for (Obiekt it : ElementyGry) {
            if (it.getNazwa().equals(nazwa)) {
                return it;
            }
        }
        System.out.println("nie znaleziono !");
        return null;
    }



    public String czyMozliwaPodmiana(String text) {
        boolean wynik = false;

        for (String key : zmienne.keySet()) {
            // System.out.println("key: "+key);
            if (text.contains("$" + key)) {
                wynik = true;
            }

        }
        return text;
    }


    public int getZmienna(String nazwa) {
        return zmienne.get(nazwa);
    }

    public void addZmienna(String nazwa, int wartosc) {
        if (!czyNazwaZajeta(nazwa)) {
            zmienne.put(nazwa, wartosc);
        } else {
            delZmienna(nazwa);
            addZmienna(nazwa, wartosc);
        }
    }

    public void delZmienna(String nazwa) {
        zmienne.remove(nazwa);
    }

    public boolean czyNazwaZajeta(String nazwa) {
        return zmienne.containsKey(nazwa);
    }


}
