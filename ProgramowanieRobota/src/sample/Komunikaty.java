package sample;

import javafx.application.Platform;
import javafx.scene.control.Alert;

public class Komunikaty {

    public static void wyswietlOkienkoALERTprzedSkompilowaniem() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("STOP");
        alert.setHeaderText("STOP");
        alert.setContentText("Przed uruchomieniem należy skompilować program!");
        alert.showAndWait();
    }

    public static void wyswietlOkienkoALERTBlad(int linia) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("BLAD");
        alert.setHeaderText("Komplilacja nieudana");
        alert.setContentText("Blad w poleceniu " + linia);
        alert.showAndWait();
    }

    public static void wyswietlOkienkoALERTBladNieMa(int linia) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("BLAD STALYCH");
        alert.setHeaderText("Komplilacja nieudana");
        alert.setContentText("Blad w poleceniu " + linia +" Nie ma takiego obiektu");
        alert.showAndWait();
    }
    public static void wyswietlOkienkoALERTBrakPolecen() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Brak polecen");
        alert.setHeaderText("Nie ma poleceń do kompilacji");
        alert.showAndWait();
    }

    public static void wyswietlOkienkoKompilacjaSukces() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Sukces");
        alert.setHeaderText("Kompilacja zakonczona pomyślnie");
        alert.showAndWait();
    }

    public static void wyswietlOkienkoPorazka() {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Porazka");
            alert.setHeaderText("Kontakt z obiektem");
            alert.setContentText("Twoj robot zderzyl sie z innym obiektem\n i zostal zniszczony");
            alert.showAndWait();
        });
    }


    public static void wyswietlOkienkoWygrales() {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Wygrales");
            alert.setHeaderText("Poziom ukonczony");
            alert.setContentText("Robot doszedl do dzrzwi");
            alert.showAndWait();
        });


    }
}
