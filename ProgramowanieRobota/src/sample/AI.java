package sample;

import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;
import sample.objects.Obiekt;
import sample.objects.ObiektWroga;

import java.util.Random;

public class AI {


     int a=0;


     //program małbt
    public void progNr1(ObiektWroga wrog) {

        //wyczyszczenie pamieci polecen robota
        wrog.getPamiecPolecen().clear();


        //dzialanie matematyczne pomocnicze tutaj losowanie
        int los = new Random().nextInt(wrog.getZwrotnosc())+1;
        //  System.out.println("wylkosowana wartosc"+los);

        //przygotowanie komend
        KomendaRobota idz1 = new KomendaRobota(0, 1);
        idz1.setArg(0,wrog.getEnergia()+"");

        // System.out.println("energia wroga to "+wrog.getEnergia());

        KomendaRobota obr1 = new KomendaRobota(1, 1);
        obr1.setArg(0,los+"");


        //0 - komenda naprzod
        //1 - komenda obroc itp.
        // ..... (patrz w GameManager, definicja komend kolejnosc komend to jest jej nr operacji)
        // mozesz podejrzec tez w programie gracza po jego kompilacji co sie wyswietli w konsoli beda to te kody wlasnie kody


        //dodanie komend do listy polecen wykonywanych
        wrog.getPamiecPolecen().add(idz1);
        wrog.getPamiecPolecen().add(obr1);


    }




    //program strzelba, auto , pociag
    public  void progNr2(ObiektWroga wrog) {

        wrog.getPamiecPolecen().clear();

        KomendaRobota sidz = new KomendaRobota(0, 1);
        KomendaRobota sobroc = new KomendaRobota(1, 1);

        sidz.setArg(0,wrog.getEnergia()+"");
        sobroc.setArg(0,wrog.getZwrotnosc()+"");

        wrog.getPamiecPolecen().add(sidz);
        wrog.getPamiecPolecen().add(sobroc);


    }

    //program bazuka
    public void progNr3(ObiektWroga wrog) {
        wrog.obroc(270);
        //a=0
        wrog.getPamiecZmiennych().addZmienna("a", 0);

        //jeżeli(a==0){
        KomendaRobota jż = new KomendaRobota(5,3);
        jż.setArg(0,"$a");
        jż.setArg(1,"<");
        jż.setArg(2,"1");

        //naprzod(173)
        KomendaRobota idz = new KomendaRobota(0, 1);
        idz.setArg(0, "173");


        //obroc(90)
        KomendaRobota obr = new KomendaRobota(1, 1);
        obr.setArg(0, "90");

        KomendaRobota idz2 = new KomendaRobota(0, 1);
        idz2.setArg(0, "600");


        //a=$a+1
        KomendaRobota inc = new KomendaRobota(1, 4);
        inc.setTyp(2); //UWAGA NA TO !!!
        inc.setArg(0,"a");
        inc.setArg(1,"$a");
        inc.setArg(2,"+");
        inc.setArg(3,"1");

        KomendaRobota koniec = new KomendaRobota(3, 0);


        KomendaRobota jz1 = new KomendaRobota(5,3);
        jz1.setArg(0,"$a");
        jz1.setArg(1,"==");
        jz1.setArg(2,"1");


        KomendaRobota obr2 = new KomendaRobota(1, 1);
        obr2.setArg(0, "90");

        KomendaRobota idz3 = new KomendaRobota(0, 1);
        idz3.setArg(0, "100");

        KomendaRobota koniec2 = new KomendaRobota(3, 0);


        wrog.getPamiecPolecen().add(jż); //jezeli a=0
        wrog.getPamiecPolecen().add(idz); //idz 173
        wrog.getPamiecPolecen().add(obr);  //obroc 90
        wrog.getPamiecPolecen().add(idz2); //idz 173
        wrog.getPamiecPolecen().add(inc);// zwieksz o 1 a++
        wrog.getPamiecPolecen().add(koniec);
        //  System.out.println("warotsc a to " +a);
        wrog.getPamiecPolecen().add(jz1); //jezeli a==1
        wrog.getPamiecPolecen().add(obr2);
        wrog.getPamiecPolecen().add(idz3); // idz 100
        wrog.getPamiecPolecen().add(koniec2);


        wrog.getPamiecPolecen().add(koniec);



    }


    public  void progNr4(ObiektWroga wrog) {

        wrog.getPamiecZmiennych().addZmienna("a", 0);

        //jeżeli(a==0){
        KomendaRobota jż = new KomendaRobota(5,3);
        jż.setArg(0,"$a");
        jż.setArg(1,"<");
        jż.setArg(2,"6");

        //naprzod(173)
        KomendaRobota idz = new KomendaRobota(0, 1);
        idz.setArg(0, wrog.getEnergia()+"");

        //obroc(90)
        KomendaRobota obr = new KomendaRobota(1, 1);
        obr.setArg(0, wrog.getZwrotnosc()+"");


        //a=$a+1
        KomendaRobota inc = new KomendaRobota(1, 4);
        inc.setTyp(2); //UWAGA NA TO !!!
        inc.setArg(0,"a");
        inc.setArg(1,"$a");
        inc.setArg(2,"+");
        inc.setArg(3,"1");

        KomendaRobota koniec = new KomendaRobota(3, 0);


        KomendaRobota jz1 = new KomendaRobota(5,3);
        jz1.setArg(0,"$a");
       jz1.setArg(1,"==");
      jz1.setArg(2,"6");


        KomendaRobota obr2 = new KomendaRobota(1, 1);
        obr2.setArg(0, "80");
//
//        KomendaRobota idz3 = new KomendaRobota(0, 1);
//        idz3.setArg(0, "100");

       KomendaRobota koniec2 = new KomendaRobota(3, 0);


        wrog.getPamiecPolecen().add(jż); //jezeli a=0
        wrog.getPamiecPolecen().add(idz); //idz 173
        wrog.getPamiecPolecen().add(obr);  //obroc 90
        wrog.getPamiecPolecen().add(inc);// zwieksz o 1 a++
        wrog.getPamiecPolecen().add(koniec);
        //  System.out.println("warotsc a to " +a);
      wrog.getPamiecPolecen().add(jz1); //jezeli a==1
        wrog.getPamiecPolecen().add(obr2);
        wrog.getPamiecPolecen().add(inc);
////        wrog.getPamiecPolecen().add(idz3); // idz 100
     wrog.getPamiecPolecen().add(koniec2);


        wrog.getPamiecPolecen().add(koniec);

    }


    public  void progNr5(ObiektWroga wrog) {


        wrog.getPamiecZmiennych().addZmienna("a", 0);

        //jeżeli(a==0){
        KomendaRobota jż = new KomendaRobota(5,3);
        jż.setArg(0,"$a");
        jż.setArg(1,"<");
        jż.setArg(2,"1");

        //naprzod(173)
        KomendaRobota idz = new KomendaRobota(0, 1);
        idz.setArg(0, wrog.getEnergia()+"");

        //obroc(90)
        KomendaRobota obr = new KomendaRobota(1, 1);
        obr.setArg(0, wrog.getZwrotnosc()+"");




        KomendaRobota koniec = new KomendaRobota(3, 0);





        wrog.getPamiecPolecen().add(jż); //jezeli a=0
        wrog.getPamiecPolecen().add(idz); //idz 173
        wrog.getPamiecPolecen().add(obr);  //obroc 90
        wrog.getPamiecPolecen().add(koniec);


        wrog.getPamiecPolecen().add(koniec);


    }

    public  void progNr6(ObiektWroga wrog) {

        wrog.getPamiecPolecen().clear();

        KomendaRobota sidz = new KomendaRobota(0, 1);
     //   KomendaRobota sidz2 = new KomendaRobota(0, 1);

        sidz.setArg(0,wrog.getEnergia()+"");
//        sidz2.setArg(0,"-"+ wrog.getEnergia()+"");
//        System.out.println("-"+ wrog.getEnergia()+"");

        wrog.getPamiecPolecen().add(sidz);
   //     wrog.getPamiecPolecen().add(sidz2);


    }

    public  void progNr7(ObiektWroga wrog) {
    }


    public  void progNr8(ObiektWroga wrog) {

    }

    public  void progNr9(ObiektWroga wrog) {

    }
}
