package sample.objects;

import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import sample.DaneGry;
import sample.PamiecPolecen;
import sample.PamiecZmiennych;

import static java.lang.Math.cos;
import static java.lang.Math.sin;


public class Obiekt {

    private String nazwa;
    private Label obraz;
    private double pozX;
    private double pozY;
    private double szerokosc;
    private double wysokosc;
    private int przesuniecieobrazka =0;
    double zwrot;

    private PamiecZmiennych pamiecZmiennych = new PamiecZmiennych();
    private PamiecPolecen pamiecPolecen = new PamiecPolecen(pamiecZmiennych);

    public Obiekt(Label obraz) {
        nazwa = obraz.getId();
        this.obraz = obraz;
        pozX = obraz.getLayoutX();
        pozY = obraz.getLayoutY();
        szerokosc = obraz.getPrefWidth();
        wysokosc = obraz.getPrefHeight();
        zwrot = obraz.getRotate();

    }


    public int getPrzesuniecieobrazka() {
        return przesuniecieobrazka;
    }

    public void setPrzesuniecieobrazka(int przesuniecieobrazka) {
        this.przesuniecieobrazka = przesuniecieobrazka;
    }

    public PamiecZmiennych getPamiecZmiennych() {
        return pamiecZmiennych;
    }


    public PamiecPolecen getPamiecPolecen() {
        return pamiecPolecen;
    }


    public String getNazwa() {
        return nazwa;
    }


    // zwaraca ostatani element listy pentle


    public double getZwrot() {
        return zwrot;
    }

    public void setZwrot(int zwrot) {
        this.zwrot = zwrot;
    }


    // pozycja X obiektu
    public void setPozY(double pozY) {
        this.pozY = pozY;
    }

    // pozycja Y obiektu
    public void setPozX(double pozX) {
        this.pozX = pozX;
    }


    public void setObraz(Label obraz) {
        this.obraz = obraz;
    }

    Label getObraz() {
        return obraz;
    }


    public double getWysokosc() {
        return wysokosc;
    }

    public double getSzerokosc() {
        return szerokosc;
    }

    double getPozX() {
        return pozX;
    }

    double getPozY() {
        return pozY;
    }


    // funkcje

    public void obroc(int kat) {
        setZwrot((int) zwrot + kat);

    }

    public void naprzod(double odl) {
        double a = Math.toRadians(obraz.getRotate());
        setPozX(obraz.getLayoutX() + (cos(a) * odl));
        setPozY(obraz.getLayoutY() + (sin(a) * odl));

     //   if(nazwa.equals("malpa12"))
     //   System.out.println(nazwa+"ffff"+ obraz.getLayoutX());


    }

    public void aktualizujObraz() {
        obraz.setLayoutY(pozY);
        obraz.setLayoutX(pozX);
        obraz.setRotate(zwrot);

    }


    public boolean jestKolizja(Obiekt o2) {
        boolean wX = (getPozX() < (o2.getPozX() + o2.getSzerokosc()) && ((getPozX() + getSzerokosc()) > o2.getPozX()));
        boolean wY = ((getPozY() + getWysokosc()) > o2.getPozY()) && (getPozY() < (o2.getPozY() + o2.getWysokosc()));

        return wX && wY && o2 != this;
    }


    public void ustawWsp(int X, int Y) {
        setPozX(X);
        setPozY(Y);
    }


    public void wypiszWsp() {
        System.out.println("X: " + getPozX() + " Y:" + getPozY() + "Zwrot" + getZwrot());
    }


    public double odlegoscX(Obiekt o2) {
        return Math.abs(pozX - o2.pozX);
    }

    public double odlegoscY(Obiekt o2) {
        return Math.abs(pozY - o2.pozY);
    }


}
