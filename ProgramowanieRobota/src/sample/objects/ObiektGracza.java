package sample.objects;

import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import sample.DaneGry;

import java.util.List;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

public class ObiektGracza extends Obiekt {


    public ObiektGracza(Label obraz) {
        super(obraz);

    }


    public boolean czyPokrokuWyjdzieZaEkran() {
        double a = Math.toRadians(getObraz().getRotate());
        double TpozX = getPozX() + cos(a);
        double TpozY = getPozY() + sin(a);

        if (TpozX + getSzerokosc() >= DaneGry.WidthMap || TpozX - getSzerokosc() <= 0) {
            return true;
        } else return TpozY + getWysokosc() >= DaneGry.HeightMap || TpozY - getWysokosc() <= 0;
    }

    public void naprzod() {
       if(!czyPokrokuWyjdzieZaEkran()){
           naprzod(1);
       }

    }

    public int zderzenieGracza(List<Obiekt> ElementyGry) {
        for (Object it : ElementyGry) {

                if (jestKolizja(((Obiekt) it))) {

                    if(it instanceof ObiektZwyciestwa){
                        return 2;
                    }
                    else{
                        return 1;
                    }

                }
        }
        return 0;
    }


}
