package sample.objects;

import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import sample.AI;
import sample.DaneGry;
import sample.GameManager;

import java.util.Random;

import static java.lang.Math.cos;
import static java.lang.Math.sin;


public class ObiektWroga extends Obiekt {

    double szybkosc;
    int zwrotnosc;
    int los_pom;
    int energia;
    int NrprogramuAI;
    AI ai ;


    public int getLos_pom() {
        return los_pom;
    }

    public void setLos_pom(int los_pom) {
        this.los_pom = los_pom;
    }

    public int getProgramAI() {
        return NrprogramuAI;
    }

    public void setProgramAI(int programAI) {
        this.NrprogramuAI = programAI;
    }


    public ObiektWroga(Label obraz, double szybkosc, int zwrotnosc, int energia, int NrprogramuAI) {
        super(obraz);
        this.szybkosc = szybkosc;
        this.zwrotnosc = zwrotnosc;
        this.energia = energia;
        this.NrprogramuAI = NrprogramuAI;
        ai= new AI();
    }

    public double getSzybkosc() {
        return szybkosc;
    }

    public int getZwrotnosc() {
        return zwrotnosc;
    }


    public void setSzybkosc(int szybkosc) {
        this.szybkosc = szybkosc;
    }

    public void setZwrotnosc(int zwrotnosc) {
        this.zwrotnosc = zwrotnosc;
    }

    public void setEnergia(int energia) {
        this.energia = energia;
    }


    public int getEnergia() {
        return energia;
    }


    public void losujObrot() {
        Random ML = new Random();
        los_pom = ML.nextInt(zwrotnosc);
    }


    public void naprzod() {
        if (czyMozliwyKrok()) {
            naprzod(szybkosc);
        }
    }// jest 1 szybkosc


    public boolean czyMozliwyKrok() {
        boolean w1 = !this.czyPokrokuWyjdzieZaEkran();
      //  System.out.println("Nie wyjdzie za ekran = "+w1);

       boolean w2 = (this.czyZderzeniePoKroku() == null) || (this.czyZderzeniePoKroku() == GameManager.ElementyGry.get(0));
      //  boolean w2 = (this.czyZderzeniePoKroku() == null);
        // boolean w2 =  (this.czyZderzeniePoKroku() == DaneGry.ElementyGry.get(0));
       // System.out.println("Nie zderzy sie z zadnym obiektem(wyjawszy gracza) = "+w2);

        // System.out.println(wrog.getEnergia()+ " "+e);
        //  System.out.println(wrog.getEnergia() > e);
         return w1 && w2;
    }


    // jeskli jest true to wyszedl za mape
    public boolean czyPokrokuWyjdzieZaEkran() {
        double a = Math.toRadians(getObraz().getRotate());
        double TpozX = getPozX() + cos(a) * szybkosc;
        double TpozY = getPozY() + sin(a) * szybkosc;

        if (TpozX + getSzerokosc() >= DaneGry.WidthMap || TpozX - getSzerokosc() <= 0) {
            return true;
        } else return TpozY + getWysokosc() >= DaneGry.HeightMap || TpozY - getWysokosc() <= 0;
    }


    public Obiekt czyZderzeniePoKroku() {

        double a = Math.toRadians(getObraz().getRotate());

        double pozX = this.getPozX();
        double pozY = this.getPozY();

        double TpozX = getPozX() + cos(a) * szybkosc;
        double TpozY = getPozY() + sin(a) * szybkosc;

       setPozX(TpozX);
       setPozY(TpozY);


        for (Object it : GameManager.ElementyGry) {
       //     System.err.println("ob"+((Obiekt)it).getNazwa());
            if (jestKolizja(((Obiekt) it)) == true) {
                setPozX(pozX);
                setPozY(pozY);
                return (Obiekt) it;
            }
        }

        setPozX(pozX);
        setPozY(pozY);

        return null;
    }


    public void Programuj() {
        switch (NrprogramuAI) {
            case 1:
                ai.progNr1(this);
                break;
            case 2:
                ai.progNr2(this);
                break;
            case 3:
                ai.progNr3(this);
                break;
            case 4:
                ai.progNr4(this);
                break;
            case 5:
                ai.progNr5(this);
                break;
            case 6:
                ai.progNr6(this);
                break;
            case 7:
                ai.progNr7(this);
                break;
            case 8:
                ai.progNr8(this);
                break;
            case 9:
                ai.progNr9(this);
                break;

            default:
                System.err.println("Nie załadowano AI");
                break;

        }
    }
}
