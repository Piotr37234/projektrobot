package sample.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import sample.DaneGry;
import sample.GameManager;
import sample.objects.Obiekt;
import sample.objects.ObiektGracza;
import sample.objects.ObiektWroga;
import sample.objects.ObiektZwyciestwa;

import java.util.ArrayList;
import java.util.List;

public class ControllerLv3 {

    @FXML
    private TextArea konsola;
    @FXML
    private TextArea kons;
    @FXML
    private Label czlowiek1;


    @FXML
    private Label helicopter31;
    @FXML
    private Label jeep31;
    @FXML
    private ImageView rails31;
    @FXML
    private ImageView rails32;
    @FXML
    private ImageView rails33;
    @FXML
    private ImageView rails34;
    @FXML
    private Label train31;
    @FXML
    private Label train32;
    @FXML
    private Label helicopter32;
    @FXML
    private ImageView pistolet31;
    @FXML
    private Label czolg31;
    @FXML
    private ImageView bazuka32;
    @FXML
    private Label jeep33;
    @FXML
    private Label jeep32;
    @FXML
    private Label baza31;
    @FXML
    private Label drzwi31;
//    @FXML
//    private ImageView tlo;


    //tu dodaj def obiektow


    public static List<Obiekt> ElementyGry = new ArrayList<Obiekt>();

    ObiektGracza USER;
    GameManager gra;

    @FXML
    public void initialize() {
        ElementyGry.add(new ObiektGracza(czlowiek1));
        USER = (ObiektGracza) ElementyGry.get(0);


       ElementyGry.add(new Obiekt(baza31));
        ElementyGry.add(new ObiektWroga(jeep31, 1, 1, 1, 1));
        ElementyGry.add(new ObiektWroga(train31, 2.5, 180, 500, 2));
        ElementyGry.add(new ObiektWroga(train32, 2.5, 180, 300, 2));
        ElementyGry.add(new ObiektWroga(jeep32, 2, -10, 40, 4));
        ElementyGry.add(new ObiektWroga(jeep33, 10, 180, 50, 2));
        ElementyGry.add(new ObiektWroga(czolg31, 0.8, 180, 550, 2));
        ElementyGry.add(new ObiektWroga(helicopter31, 1, -5, 35, 5));
        ElementyGry.add(new ObiektWroga(helicopter32, 0.7, -90, 600, 5));
        //ustawic
        ElementyGry.add(new ObiektZwyciestwa(drzwi31));


        gra = new GameManager(ElementyGry, USER, kons, konsola, 3);
        gra.start();


    }


    public void kompiluj() {
        gra.kompiluj();
    }

    public void czyscKonsole() {
        gra.czyscKonsole();
    }

    public void kliknijUruchom() {
        gra.kliknijUruchom();
    }


    public void stopProgram() {
        gra.stopProgram();
    }

    public void oznaczNieskompilowany() {
        gra.oznaczNieskompilowany();
    }


}
