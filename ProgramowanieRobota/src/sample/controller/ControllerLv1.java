package sample.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import sample.DaneGry;
import sample.GameManager;
import sample.objects.Obiekt;
import sample.objects.ObiektGracza;
import sample.objects.ObiektWroga;
import sample.objects.ObiektZwyciestwa;

import java.util.ArrayList;
import java.util.List;

public class ControllerLv1 {

    @FXML
    private Label czlowiek1;
    @FXML
    private Label strzelba11;
    @FXML
    private Label strzelba12;
    @FXML
    private Label bazuka11;
    @FXML
    private Label malpa11;
    @FXML
    private Label malpa12;
    @FXML
    private Label drzewo11;
    @FXML
    private Label drzewo12;
    @FXML
    private Label drzewo13;
    @FXML
    private Label drzewo14;
    @FXML
    private Label drzewo15;
    @FXML
    private TextArea konsola;
    @FXML
    private TextArea kons;
    @FXML
    private Label bomba11;
    @FXML
    private Label bomba12;
    @FXML
    private Label drzwi11;





    public static List<Obiekt> ElementyGry = new ArrayList<Obiekt>();

    ObiektGracza USER;
    GameManager gra;

    @FXML
    public void initialize() {

      ElementyGry.add(new ObiektGracza(czlowiek1));
       USER = (ObiektGracza) ElementyGry.get(0);

       ElementyGry.add(new ObiektWroga(strzelba11, 1, 180, 650, 2));   //energia naprzod //zwrotonsc oboort
        ElementyGry.add(new ObiektWroga(strzelba12, 1 , 180, 350, 2));
// gorna granica loswego obrotu
        ElementyGry.add(new ObiektWroga(bazuka11, 1, 360, 173, 3));

        ElementyGry.add(new ObiektWroga(malpa11, 1, 360, 60, 1));  //energia naprzod , zwrotnosc gorna grancanioca randa

        ElementyGry.add(new ObiektWroga(malpa12, 5, 360, 60, 1));

        ElementyGry.add(new Obiekt(drzewo11));
        ElementyGry.add(new Obiekt(drzewo12));

        ElementyGry.add(new Obiekt(drzewo13));
        ElementyGry.add(new Obiekt(drzewo14));
        ElementyGry.add(new Obiekt(drzewo15));
        ElementyGry.add(new Obiekt(bomba11));
        ElementyGry.add(new Obiekt(bomba12));


        ElementyGry.add(new ObiektZwyciestwa(drzwi11));



        gra = new GameManager(ElementyGry, USER, kons, konsola, 1);
        gra.start();
    }


    public void kompiluj() {
        gra.kompiluj();
    }

    public void czyscKonsole() {
        gra.czyscKonsole();
    }

    public void kliknijUruchom() {
        gra.kliknijUruchom();
    }


    public void stopProgram() {
        gra.stopProgram();
    }

    public void oznaczNieskompilowany() {
        gra.oznaczNieskompilowany();
    }


}
