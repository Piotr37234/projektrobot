package sample.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import sample.DaneGry;


public class WyborLvl {

    @FXML
    javafx.scene.image.ImageView mlvl1;
    @FXML
    javafx.scene.image.ImageView mlvl2;
    @FXML
    javafx.scene.image.ImageView mlvl3;

    @FXML
    public void initialize(){

        if (!DaneGry.ukonczonePoziomy[1]) {
          mlvl2.setOpacity(0.1);
        }
        if (!DaneGry.ukonczonePoziomy[2]) {
            mlvl3.setOpacity(0.1);
        }

    }

    public void kliknijWroc() {
        DaneGry.poziomy.close();
        DaneGry.menu.show();
    }


    public void Lv1() {
        DaneGry.lv1 = DaneGry.ustawStage(new FXMLLoader(getClass().getResource("../screen/lv1.fxml")), 1600, 900, "Lv 1");
        DaneGry.lv1.show();
        DaneGry.poziomy.close();
    }

    public void Lv2() {
        if (DaneGry.ukonczonePoziomy[1]) {
            DaneGry.lv2 = DaneGry.ustawStage(new FXMLLoader(getClass().getResource("../screen/lv2.fxml")), 1600, 900, "Lv 2");
            DaneGry.lv2.show();
            DaneGry.poziomy.close();
        }
    }

    public void Lv3() {
        if (DaneGry.ukonczonePoziomy[2]) {
            DaneGry.lv3 = DaneGry.ustawStage(new FXMLLoader(getClass().getResource("../screen/lv3.fxml")), 1600, 900, "Lv 3");
            DaneGry.lv3.show();
            DaneGry.poziomy.close();
        }
    }


    public void wrocDoMenu() {
        DaneGry.poziomy.close();
        DaneGry.menu.show();
    }


}
