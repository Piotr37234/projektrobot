package sample.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import sample.DaneGry;
import sample.GameManager;
import sample.objects.Obiekt;
import sample.objects.ObiektGracza;
import sample.objects.ObiektWroga;
import sample.objects.ObiektZwyciestwa;

import java.util.ArrayList;
import java.util.List;

public class ControllerLv2 {

    @FXML
    private TextArea konsola;
    @FXML
    private TextArea kons;
    @FXML
    private Label czlowiek1;



    @FXML
    private ImageView ul22;
    @FXML
    private ImageView ul21;
    @FXML
    private Label auto21;
    @FXML
    private Label auto22;
    @FXML
    private Label drzewo23;
    @FXML
    private Label drzewo22;
    @FXML
    private Label dom21;
    @FXML
    private Label drzewo24;
    @FXML
    private Label basen21;
    @FXML
    private Label zielony_diament;
    @FXML
    private Label Niebieski_diament;
    @FXML
    private Label drzewo21;

 //   @FXML
 //   private ImageView buckground2;


    public static List<Obiekt> ElementyGry = new ArrayList<Obiekt>();

    ObiektGracza USER;
    GameManager gra;

    @FXML
    public void initialize() {
        ElementyGry.add(new ObiektGracza(czlowiek1));
        USER = (ObiektGracza) ElementyGry.get(0);




        ElementyGry.add(new ObiektWroga(auto21, 2, 180, 280, 2));
        ElementyGry.add(new ObiektWroga(auto22, 1, 180, 450, 2));
   //     ElementyGry.add(new Obiekt(ul22));
     //   ElementyGry.add(new Obiekt(ul21));
        ElementyGry.add(new Obiekt(zielony_diament));
        ElementyGry.add(new Obiekt(Niebieski_diament));
        ElementyGry.add(new Obiekt(drzewo21));
        ElementyGry.add(new Obiekt(basen21));
        ElementyGry.add(new Obiekt(drzewo24));
        ElementyGry.add(new Obiekt(dom21));
        ElementyGry.add(new Obiekt(drzewo22));
        ElementyGry.add(new Obiekt(drzewo23));
     //   ElementyGry.add(new Obiekt(buckground2));
   //     ElementyGry.add(new ObiektZwyciestwa(drzwi21));




//
//        ElementyGry.add(new ObiektWroga(strzelba11, 1, 360, 30, 1));
//        ElementyGry.add(new ObiektWroga(strzelba12, 1, 360, 30, 1));
//        ElementyGry.add(new ObiektWroga(bazuka11, 1, 360, 30, 1));
//        ElementyGry.add(new ObiektWroga(malpa11, 1, 360, 30, 1));
//        ElementyGry.add(new ObiektWroga(malpa12, 1, 360, 30, 1));
//
//        ElementyGry.add(new Obiekt(ul22));
//        ElementyGry.add(new Obiekt(ul21));
//        ElementyGry.add(new Obiekt(drzewo13));
//        ElementyGry.add(new Obiekt(drzewo14));
//        ElementyGry.add(new Obiekt(drzewo15));
//        ElementyGry.add(new Obiekt(bomba11));
//        ElementyGry.add(new Obiekt(bomba12));
//        ElementyGry.add(new ObiektZwyciestwa(drzwi11));

        //tu dodaj  obiekty

        gra = new GameManager(ElementyGry, USER, kons, konsola, 2);
        gra.start();



    }




    public void kompiluj() {
        gra.kompiluj();
    }

    public void czyscKonsole(){
        gra.czyscKonsole();
    }

    public void kliknijUruchom(){
        gra.kliknijUruchom();
    }


    public void stopProgram(){
        gra.stopProgram();
    }

    public void oznaczNieskompilowany(){
        gra.oznaczNieskompilowany();
    }


}
