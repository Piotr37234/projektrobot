package sample;

import sample.objects.Obiekt;

import java.util.ArrayList;
import java.util.List;

public class PamiecPolecen {
    public List<KomendaRobota> skompilowaneKomendy = new ArrayList<KomendaRobota>();
    public List<Pentla> getPentle() {
        return pentle;
    }
    public void wypiszPentle() {
        System.out.println("######");
        for(int i=0;i<pentle.size();i++)
        pentle.get(i).p();
        System.out.println("######");
    }


    public void setPentle(List<Pentla> pentle) {
        this.pentle = pentle;
    }
    public List<Pentla> pentle = new ArrayList<Pentla>();
    public PamiecZmiennych pam;
    boolean wstrzymanie = false;

    public PamiecPolecen(PamiecZmiennych pam){
        this.pam = pam;
    }

    public void add(KomendaRobota kom){
        skompilowaneKomendy.add(kom);
    }

    public void rozkodujAktualnaKomende(List<Obiekt> ElementyGry){
       getAktualnaKomenda().rozkoduj(pam,ElementyGry);
    }

    public int size(){
        return skompilowaneKomendy.size();
    }


    public void p() {
        for (Object it : skompilowaneKomendy) {
            ((KomendaRobota) it).p();
        }
    }

    public void clear(){
        skompilowaneKomendy.clear();
    }

    public int PRZETWARZANA_LINIA = 0;
    int komendaRuchPostep;

    public Pentla getAktualnaPentla() {
        int ilosc = pentle.size();

        if (ilosc == 0) {
            return null;
        } else {
            return pentle.get(ilosc - 1);
        }

    }

    public boolean czyKoniecProgramu() {
        return skompilowaneKomendy.size() == PRZETWARZANA_LINIA;
    }

    public KomendaRobota getAktualnaKomenda() {
        return skompilowaneKomendy.get(PRZETWARZANA_LINIA);
    }


    public void nowaPentla(int licznik) {
        pentle.add(new Pentla(PRZETWARZANA_LINIA,licznik));
    }

    public void nowyWarunek(String a,String warunek,String b) {
        Pentla pw = new Pentla(PRZETWARZANA_LINIA,a,warunek,b);
        pw.aktualizuj(pam);
        pentle.add(pw);
    }


    public boolean czyPentlaWarunekOk(){
       Pentla p =  getAktualnaPentla();
        if(p==null){
            return true;
        }
        else{
            p.aktualizuj(pam);
            return p.czyWykonac();
        }

    }

    public int getKomendaRuchPostep() {
        return komendaRuchPostep;
    }

    public void setKomendaRuchPostep(int komendaRuchPostep) {
        this.komendaRuchPostep = komendaRuchPostep;
    }


    public int getPRZETWARZANA_LINIA() {
        return PRZETWARZANA_LINIA;
    }

    public void setPRZETWARZANA_LINIA(int PRZETWARZANA_LINIA) {
        this.PRZETWARZANA_LINIA = PRZETWARZANA_LINIA;
    }

    public void zerujPRZETWARZANA_LINIA() {
        PRZETWARZANA_LINIA = 0;
    }

    public void zwiekszPRZETWARZANA_LINIA() {
        PRZETWARZANA_LINIA ++;
    }






    public void nastKomenda() {


        if (komendaRuchPostep != 0) {
            komendaRuchPostep--;
        }

        if (komendaRuchPostep == 0) {
            PRZETWARZANA_LINIA++;
        }


    }


    public void iteracjaPentli() {
        int ilosc = pentle.size();
        // wskaznik ost wskazuje na ostani element listy
        Pentla ost = getAktualnaPentla();

        if (ost == null) {
            return;
        }


        //czy trwa jakas pentla
        if (ost.getLicznik() > 0) { //czy jej licznik sie nie wyzerowal, czyli trwa pentla
            //   System.out.println("petla jest i licznik = "+ost.getLicznik());
            if(!ost.czyWykonac()){
                pentle.remove(ilosc - 1);
            }


            setPRZETWARZANA_LINIA(ost.getZapamietanaLinia());

            ost.setLicznik(ost.getLicznik() - 1);
            return;
        } else {
           // System.out.println("zdejmuje pentle " + (ilosc - 1));
            pentle.remove(ilosc - 1);
        }


     //   System.err.println("Nie ma pentli do zmniejszenia !!!");


    }



}
