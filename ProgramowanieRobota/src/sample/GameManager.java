package sample;


import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import sample.objects.Obiekt;
import sample.objects.ObiektGracza;
import sample.objects.ObiektWroga;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GameManager {
    public boolean URUCHOMIONY_PROGRAM = false;
    public boolean SKOMPILOWANY = false;
    private boolean WYGRANA = false;
    private boolean PORAZKA = false;

    public static List<Obiekt> ElementyGry;
    ObiektGracza USER;
    private TextArea kons;
    private TextArea konsola;
    private int lv;


    public GameManager(List<Obiekt> ElementyGry, ObiektGracza USER, TextArea kons, TextArea konsola, int lv) {
        this.ElementyGry = ElementyGry;

        this.USER = USER;
        this.kons = kons;
        this.konsola = konsola;
        this.lv = lv;
    }

    public void start() {
        programujWrogow();
        Watek();
    }


    static String komendy[] = {
            "naprzod\\((\\d+)\\)",
            "obroc\\((\\d+)\\)",
            "powtorz\\((\\d+)\\)\\{",
            "\\}",
            "wyswietl\\((\\w+)\\)",
            "jeżeli\\((\\w+)(==|>=|<=|>|<|!=)(\\w+)\\)\\{"
    };

    static String komendy_bezTypu[] = {
            "naprzod\\((.+)\\)",
            "obroc\\((.+)\\)",
            "powtorz\\((.+)\\)\\{",
            "\\}",
            "wyswietl\\((.+)\\)",
            "jeżeli\\((.+)(==|>=|<=|>|<)(.+)\\)\\{"
    };


    static String komendyInicjalizujace[] = {
            "(\\D|\\D\\w+)=([0-9]+)",
            "(\\D|\\D\\w+)=(\\w+)(\\-|\\+|\\/|\\*)(\\w+)",
            "(\\D|\\D\\w+)=odlegloscX\\((#\\w+),(#\\w+)\\)",
            "(\\D|\\D\\w+)=odlegloscY\\((#\\w+),(#\\w+)\\)"
    };

    static String komendyInicjalizujace_bezTypu[] = {
            "(\\D|\\D\\w+)=([^+-//*#\\(\\)]+)",
            "(\\D|\\D\\w+)=([^\\+\\-\\*\\/]+)(\\-|\\+|\\/|\\*)([^\\+\\-\\*\\/]+)",
            "(\\D|\\D\\w+)=odlegloscX\\((#\\w+),(#\\w+)\\)",
            "(\\D|\\D\\w+)=odlegloscY\\((#\\w+),(#\\w+)\\)"
    };

    public void czyscKonsole() {
        kons.setText("");
    }

    public void napiszNaKonsoli(String text) {
        kons.setText(kons.getText() + text + "\n");
    }

    public void kompiluj() {
        USER.getPamiecPolecen().clear();
        USER.getPamiecZmiennych().clearZmienne();
        USER.getPamiecPolecen().zerujPRZETWARZANA_LINIA();

        String nastPolecenie = getNextCommand(konsola);

        KomendaRobota skompilowanePolecenie;
        while (nastPolecenie != null) {
            // pob kolejna komende z konsoli
            skompilowanePolecenie = textvalidate(nastPolecenie);
            //przypadek gdy jest to komenda inicjalizacyjna
            if (skompilowanePolecenie != null) {
                //dodaje zmienna do tablicy aby moc z niej pozniej wyszukiwac
                if (skompilowanePolecenie.getTyp() == 2) {
                    if (skompilowanePolecenie.getKodOperacji() < 2)
                        USER.getPamiecZmiennych().addZmienna(skompilowanePolecenie.getArg(0), skompilowanePolecenie.getArgInt(1));
                    else if (skompilowanePolecenie.getKodOperacji() == 3) {
                        Obiekt o1 = wezObiekt(skompilowanePolecenie.getArg(1));
                        Obiekt o2 = wezObiekt(skompilowanePolecenie.getArg(2));
                        if(o1==null || o2==null){
                            Komunikaty.wyswietlOkienkoALERTBladNieMa(USER.getPamiecPolecen().getPRZETWARZANA_LINIA());
                            return;
                        }
                        USER.getPamiecZmiennych().addZmienna(skompilowanePolecenie.getArg(0), (int) odlegloscXmiedzyDwomaObiektami(o1, o2));
                    } else {
                        Obiekt o1 = wezObiekt(skompilowanePolecenie.getArg(1));
                        Obiekt o2 = wezObiekt(skompilowanePolecenie.getArg(2));
                        if(o1==null || o2==null){
                            Komunikaty.wyswietlOkienkoALERTBladNieMa(USER.getPamiecPolecen().getPRZETWARZANA_LINIA());
                            return;
                        }

                        USER.getPamiecZmiennych().addZmienna(skompilowanePolecenie.getArg(0), (int) odlegloscYmiedzyDwomaObiektami(o1, o2));
                    }
                }
                USER.getPamiecPolecen().add(skompilowanePolecenie);
                //dodaje polecenie
            } else {

                //tu powinno inf uzytkownika o bledzie!!!

                Komunikaty.wyswietlOkienkoALERTBlad(USER.getPamiecPolecen().getPRZETWARZANA_LINIA());

                return;


            }


            nastPolecenie = getNextCommand(konsola);
        }


        if (USER.getPamiecPolecen().skompilowaneKomendy.size() != 0 || USER.getPamiecZmiennych().getZmienne().size() != 0) { // jest nullem a wiec nie ma wiecej textu w konsoli , kompilacja zakonczona

            Komunikaty.wyswietlOkienkoKompilacjaSukces();
           // USER.getPamiecPolecen().p();
            oznaczSkomilowany();
        } else {
            Komunikaty.wyswietlOkienkoALERTBrakPolecen();
        }


    }

    public void oznaczSkomilowany() {
        SKOMPILOWANY = true;
    }

    public void oznaczNieskompilowany() {
        SKOMPILOWANY = false;
    }

    public void kliknijUruchom() {


        if (!SKOMPILOWANY) {
            Komunikaty.wyswietlOkienkoALERTprzedSkompilowaniem();
            return;
        }
        startProgram();
    }

    public Obiekt wezObiekt(String nazwa) {
        for (Obiekt it : ElementyGry) {
            if (it.getNazwa().equals(nazwa)) {
                return it;
            }
        }
        System.out.println("nie znaleziono !");
        return null;
    }

    private String getNextCommand(TextArea konsola) {
        String komenda = null;
        USER.getPamiecPolecen().zwiekszPRZETWARZANA_LINIA();

       // System.out.println("konsola: " + konsola.getText());
        StringTokenizer calyTekst = new StringTokenizer(konsola.getText());
        try {
            for (int i = 0; i < USER.getPamiecPolecen().getPRZETWARZANA_LINIA(); i++) {
                komenda = calyTekst.nextToken("\n ");
            }
        } catch (NoSuchElementException e) {
            return null;
        }
        return komenda;
    }

    public KomendaRobota wytworzKomende(String text, String bezTypu[], String wlasciwe[], int typ) {
        Matcher M = null;
        int i;
        KomendaRobota nowa = null;
        for (i = 0; i < wlasciwe.length; i++) {
            Pattern bezTypuW = Pattern.compile(bezTypu[i]);
            Pattern wlasciwaKomenda = Pattern.compile(wlasciwe[i]);
            M = bezTypuW.matcher(text);
            if (M.matches()) {
                int iloscArg = M.groupCount();
                nowa = new KomendaRobota(i, iloscArg);
                for (int l = 0; l < iloscArg; l++) {
                    nowa.setArg(l, M.group(l + 1));
                }
                nowa.rozkoduj(USER.getPamiecZmiennych(), ElementyGry);

                text = USER.getPamiecZmiennych().ciagZPodmienionymiZmiennymi(text);


                nowa.p();
                M = wlasciwaKomenda.matcher(text);
                if (M.matches()) {
                    nowa.setTyp(typ);
                    return nowa;
                }
            }
        }
        return null;

    }

    private KomendaRobota textvalidate(String text) {
        KomendaRobota kom;
        //String pom = text;
        //text = USER.getPamiecZmiennych().ciagZPodmienionymiZmiennymi(text);
        kom = wytworzKomende(text, komendy_bezTypu, komendy, 1);
        if (kom == null) {
            kom = wytworzKomende(text, komendyInicjalizujace_bezTypu, komendyInicjalizujace, 2);
        }
        if (kom != null) {
            return kom;
        } else {
            System.err.println("Nieznane Polecenie !!");
        }
        return null;
    }

    public void startProgram() {
        URUCHOMIONY_PROGRAM = true;
        USER.getPamiecPolecen().zerujPRZETWARZANA_LINIA();
        Platform.runLater(() -> {
            napiszNaKonsoli("Program wystartowal");


        });
    }

    public void stopProgram() {
        URUCHOMIONY_PROGRAM = false;
        Platform.runLater(() -> {
            napiszNaKonsoli("Program zatrzymany");
        });
    }

    public void endProgram() {
        URUCHOMIONY_PROGRAM = false;
        Platform.runLater(() -> {
            napiszNaKonsoli("Program zakonczony");
        });
    }

    public double odlegloscXmiedzyDwomaObiektami(Obiekt o1, Obiekt o2) {
        return o1.odlegoscX(o2);
    }

    public double odlegloscYmiedzyDwomaObiektami(Obiekt o1, Obiekt o2) {
        return o1.odlegoscY(o2);
    }

    public void uruchomPolecenieGracza() {
        int cococo;
        int typ;
        KomendaRobota AKTUALNA = USER.getPamiecPolecen().getAktualnaKomenda();
        USER.getPamiecPolecen().rozkodujAktualnaKomende(ElementyGry);
        cococo = AKTUALNA.getKodOperacji();
        typ = AKTUALNA.getTyp();


        if (!USER.getPamiecPolecen().czyPentlaWarunekOk()) {
            if ((cococo == 2 || cococo == 3 || cococo == 5) && typ == 1) {

            } else {
                cococo = -1;
            }
        }


        if (typ == 2) {
            switch (cococo) {
                case 0:
                    USER.getPamiecZmiennych().addZmienna(AKTUALNA.getArg(0), AKTUALNA.getArgInt(1));
                    break;
                case 1:
                    if (AKTUALNA.getArg(2).equals("+")) {
                        USER.getPamiecZmiennych().addZmienna(AKTUALNA.getArg(0), AKTUALNA.getArgInt(1) + AKTUALNA.getArgInt(3));
                    } else if (AKTUALNA.getArg(2).equals("-")) {
                        USER.getPamiecZmiennych().addZmienna(AKTUALNA.getArg(0), AKTUALNA.getArgInt(1) - AKTUALNA.getArgInt(3));
                    } else if (AKTUALNA.getArg(2).equals("*")) {
                        USER.getPamiecZmiennych().addZmienna(AKTUALNA.getArg(0), AKTUALNA.getArgInt(1) * AKTUALNA.getArgInt(3));
                    } else {
                        USER.getPamiecZmiennych().addZmienna(AKTUALNA.getArg(0), AKTUALNA.getArgInt(1) / AKTUALNA.getArgInt(3));
                    }
                    break;
                case 2:
                    String nazwaZmiennej = AKTUALNA.getArg(0);
                    Obiekt o1 = wezObiekt(AKTUALNA.getArg(1));
                    Obiekt o2 = wezObiekt(AKTUALNA.getArg(2));

                    int odlX = (int) (odlegloscXmiedzyDwomaObiektami(o1, o2));

                    USER.getPamiecZmiennych().addZmienna(nazwaZmiennej, odlX);
                    break;
                case 3:
                    String nazwaZmiennej2 = AKTUALNA.getArg(0);
                    Obiekt o11 = wezObiekt(AKTUALNA.getArg(1));
                    Obiekt o22 = wezObiekt(AKTUALNA.getArg(2));
                    int odlY = (int) (odlegloscYmiedzyDwomaObiektami(o11, o22));
                    USER.getPamiecZmiennych().addZmienna(nazwaZmiennej2, odlY);
                    break;
            }
        } else switch (cococo) {

            case 0:
                if (USER.getPamiecPolecen().getKomendaRuchPostep() == 0) {
                    USER.getPamiecPolecen().setKomendaRuchPostep(AKTUALNA.getArgInt(0));
                }
                USER.naprzod();
                break;
            case 1:
                USER.obroc(AKTUALNA.getArgInt(0));
                break;
            case 2:
                USER.getPamiecPolecen().nowaPentla(AKTUALNA.getArgInt(0));
                //System.out.println("pentla licznik:" + USER.getPamiecPolecen().getAktualnaPentla().getLicznik());
                //System.out.println("pentla linia:" + USER.getPamiecPolecen().getAktualnaPentla().getZapamietanaLinia());
                break;
            case 3:
                USER.getPamiecPolecen().iteracjaPentli(); //zakonczenie pentli
                break;
            case 4:
                Platform.runLater(() -> {
                    napiszNaKonsoli(AKTUALNA.getArg(0));
                });
                break;


            case 5:
                USER.getPamiecPolecen().nowyWarunek(AKTUALNA.getArg(0), AKTUALNA.getArg(1), AKTUALNA.getArg(2));
                break;

        }


        try

        {
            Thread.sleep(0,1);
        } catch (
                InterruptedException e)

        {
            e.printStackTrace();
        }

        USER.getPamiecPolecen().

                nastKomenda();

        if (USER.getPamiecPolecen().

                czyKoniecProgramu())

        {
            USER.getPamiecPolecen().zerujPRZETWARZANA_LINIA();
            endProgram();

        }

    }

    public void uruchomPolecenieWrogow() {
        int cococo;
        int typ;
        for (Object it : ElementyGry) {
            if (it instanceof ObiektWroga) {

                //

                KomendaRobota AKTUALNA = ((ObiektWroga)it).getPamiecPolecen().getAktualnaKomenda();
                ((ObiektWroga)it).getPamiecPolecen().rozkodujAktualnaKomende(ElementyGry);
                cococo = AKTUALNA.getKodOperacji();
                typ = AKTUALNA.getTyp();
                //int opoznienie =  ((ObiektWroga)it).getSzybkosc();

                if (!((ObiektWroga)it).getPamiecPolecen().czyPentlaWarunekOk()) {
                    if ((cococo == 2 || cococo == 3 || cococo == 5) && typ == 1) {

                    } else {
                        cococo = -1;
                    }
                }


                if (typ == 2) {
                    switch (cococo) {
                        case 0:
                            ((ObiektWroga)it).getPamiecZmiennych().addZmienna(AKTUALNA.getArg(0), AKTUALNA.getArgInt(1));
                            break;
                        case 1:
                            if (AKTUALNA.getArg(2).equals("+")) {
                                ((ObiektWroga)it).getPamiecZmiennych().addZmienna(AKTUALNA.getArg(0), AKTUALNA.getArgInt(1) + AKTUALNA.getArgInt(3));
                            } else if (AKTUALNA.getArg(2).equals("-")) {
                                ((ObiektWroga)it).getPamiecZmiennych().addZmienna(AKTUALNA.getArg(0), AKTUALNA.getArgInt(1) - AKTUALNA.getArgInt(3));
                            } else if (AKTUALNA.getArg(2).equals("*")) {
                                ((ObiektWroga)it).getPamiecZmiennych().addZmienna(AKTUALNA.getArg(0), AKTUALNA.getArgInt(1) * AKTUALNA.getArgInt(3));
                            } else {
                                ((ObiektWroga)it).getPamiecZmiennych().addZmienna(AKTUALNA.getArg(0), AKTUALNA.getArgInt(1) / AKTUALNA.getArgInt(3));
                            }
                            break;
                        case 2:
                            String nazwaZmiennej = AKTUALNA.getArg(0);
                            Obiekt o1 = wezObiekt(AKTUALNA.getArg(1));
                            Obiekt o2 = wezObiekt(AKTUALNA.getArg(2));

                            int odlX = (int) (odlegloscXmiedzyDwomaObiektami(o1, o2));

                            ((ObiektWroga)it).getPamiecZmiennych().addZmienna(nazwaZmiennej, odlX);
                            break;
                        case 3:
                            String nazwaZmiennej2 = AKTUALNA.getArg(0);
                            Obiekt o11 = wezObiekt(AKTUALNA.getArg(1));
                            Obiekt o22 = wezObiekt(AKTUALNA.getArg(2));
                            int odlY = (int) (odlegloscYmiedzyDwomaObiektami(o11, o22));
                            ((ObiektWroga)it).getPamiecZmiennych().addZmienna(nazwaZmiennej2, odlY);
                            break;
                    }
                } else switch (cococo) {

                    case 0:
                        if (((ObiektWroga)it).getPamiecPolecen().getKomendaRuchPostep() == 0) {
                            ((ObiektWroga)it).getPamiecPolecen().setKomendaRuchPostep(AKTUALNA.getArgInt(0));
                        }
                        ((ObiektWroga)it).naprzod();
                        break;
                    case 1:
                        ((ObiektWroga)it).obroc(AKTUALNA.getArgInt(0));
                        break;
                    case 2:
                        ((ObiektWroga)it).getPamiecPolecen().nowaPentla(AKTUALNA.getArgInt(0));
                        //System.out.println("pentla licznik:" + USER.getPamiecPolecen().getAktualnaPentla().getLicznik());
                        //System.out.println("pentla linia:" + USER.getPamiecPolecen().getAktualnaPentla().getZapamietanaLinia());
                        break;
                    case 3:
                        ((ObiektWroga)it).getPamiecPolecen().iteracjaPentli(); //zakonczenie pentli
                        break;
                    case 4:
                        Platform.runLater(() -> {
                            napiszNaKonsoli(AKTUALNA.getArg(0));
                        });
                        break;


                    case 5:
                        ((ObiektWroga)it).getPamiecPolecen().nowyWarunek(AKTUALNA.getArg(0), AKTUALNA.getArg(1), AKTUALNA.getArg(2));
                        break;

                }
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                ((ObiektWroga) it).getPamiecPolecen().nastKomenda();

                if (((ObiektWroga) it).getPamiecPolecen().czyKoniecProgramu()) {
                    ((ObiektWroga) it).getPamiecPolecen().zerujPRZETWARZANA_LINIA();
                    return;
                }

                //

            }

        }
    }

    private void programujWrogow() {
        for (Object it : ElementyGry) {
            if (it instanceof ObiektWroga) {


                ((ObiektWroga) it).Programuj();

            }
        }
    }


    public void ustawJesliToKoniecznePorazkeIZwyciestwo() {

        int test = USER.zderzenieGracza(ElementyGry);

        if (test == 1) {
            PORAZKA = true;
        } else if (test == 2) {
            WYGRANA = true;
            DaneGry.odblokujNast();
        } else {

        }

    }

    // Komunikaty.wyswietlOkienkoPorazka();
//            Platform.runLater(() -> {
//        DaneGry.lv1.close();
//        DaneGry.menu.show();
//    });


    public void closeAktualnyLv() {
        switch (lv) {
            case 1:
                DaneGry.lv1.close();
                break;
            case 2:
                DaneGry.lv2.close();
                break;
            case 3:
                DaneGry.lv3.close();
                break;
        }

    }

    public void Watek() {
        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {

                        while (true) {
                            if (WYGRANA) {
                                Platform.runLater(() -> {
                                    Komunikaty.wyswietlOkienkoWygrales();
                                    closeAktualnyLv();
                                    DaneGry.poziomy = DaneGry.ustawStage(new FXMLLoader(getClass().getResource("screen/wyborlvl.fxml")), 600, 400, "Poziomy");
                                    DaneGry.poziomy.show();
                                    //odblokuj mape
                                });
                                ElementyGry.clear();
                                return;
                            }


                            if (PORAZKA) {
                                Platform.runLater(() -> {
                                    Komunikaty.wyswietlOkienkoPorazka();
                                    closeAktualnyLv();
                                    DaneGry.menu.show();
                                });
                                ElementyGry.clear();
                                return;
                            }


                            if (URUCHOMIONY_PROGRAM) {
                                uruchomPolecenieGracza();
                            }
                            uruchomPolecenieWrogow();
                            aktualizujWszystkieObrazy();

                            ustawJesliToKoniecznePorazkeIZwyciestwo();
                        }

                        //run();
                    }


                },
                2000
        );


    }

    public void aktualizujWszystkieObrazy() {
        for (Object it : ElementyGry) {
            ((Obiekt) it).aktualizujObraz();
        }
    }

    private void wyswietlInfoObiekt(ImageView obiekt, Label info) {
        info.setPrefSize(200, 200);
        info.setText(obiekt.getId() + "\n");
        info.setText(info.getText() + "X: " + +obiekt.getLayoutX() + "\n");
        info.setText(info.getText() + "Y: " + +obiekt.getLayoutY() + "\n");
        info.setText(info.getText() + "Width: " + obiekt.getFitWidth() + "\n");
        info.setText(info.getText() + "Height: " + obiekt.getFitHeight() + "\n");
        //    info.setText(info.getText() + "Kolizja: " + jestKolizja(czlowiek, wrog1) + "\n");
    }


}



