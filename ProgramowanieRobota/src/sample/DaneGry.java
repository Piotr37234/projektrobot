package sample;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.objects.Obiekt;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DaneGry {

    public static int HeightMap = 900;
    public static int WidthMap = 1178;
    public static List<Obiekt> ElementyGry = new ArrayList<Obiekt>();

    static int IloscPoziomow = 3;

    public static Stage getMenu() {
        return menu;
    }

    public static void setMenu(Stage menu) {
        DaneGry.menu = menu;
    }

    public static Stage getLv1() {
        return lv1;
    }

    public static void setLv1(Stage lv1) {
        DaneGry.lv1 = lv1;
    }

    public static Stage getLv2() {
        return lv2;
    }

    public static void setLv2(Stage lv2) {
        DaneGry.lv2 = lv2;
    }

    public static Stage getLv3() {
        return lv3;
    }

    public static void setLv3(Stage lv3) {
        DaneGry.lv3 = lv3;
    }

    public static Stage getPoziomy() {
        return poziomy;
    }

    public static void setPoziomy(Stage poziomy) {
        DaneGry.poziomy = poziomy;
    }

    public static Stage getPomoc() {
        return pomoc;
    }

    public static void setPomoc(Stage pomoc) {
        DaneGry.pomoc = pomoc;
    }

    public static Stage menu;
    public static Stage lv1;
    public static Stage lv2;
    public static Stage lv3;

    public static Stage poziomy;
    public static Stage pomoc;


    public static boolean ukonczonePoziomy[] = new boolean[3];


    public static Stage ustawStage(FXMLLoader fxml, int width, int height, String nazwa) {
        Parent nowy = null;
        try {
            nowy = fxml.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Stage stage = new Stage();
        stage.setTitle(nazwa);
        stage.setScene(new Scene(nowy, width, height));
        return stage;
    }








    public static int ostPoziom() {
        int i = 1;
        while (ukonczonePoziomy[i]) {
            i++;
        }
        return i;
    }

    public static void odblokujNast() {
        ukonczonePoziomy[ostPoziom()]=true;
    }












}
