package sample;

import javafx.fxml.FXMLLoader;

public class ControllerMenu {


    public void start() {
        DaneGry.menu.close();
        switch (DaneGry.ostPoziom()) {
            case 1:
                DaneGry.lv1 = DaneGry.ustawStage(new FXMLLoader(getClass().getResource("screen/lv3.fxml")),1600,900,"Lv 1");
                DaneGry.lv1.show();
                break;
            case 2:
                DaneGry.lv2 = DaneGry.ustawStage(new FXMLLoader(getClass().getResource("screen/lv2.fxml")),1600,900,"Lv 2");
                DaneGry.lv2.show();
                break;
            case 3:
                DaneGry.lv3 = DaneGry.ustawStage(new FXMLLoader(getClass().getResource("screen/lv3.fxml")),1600,900,"Lv 3");
                DaneGry.lv3.show();
                break;
        }
    }

    public void poziomy() {
        DaneGry.menu.close();
        DaneGry.poziomy = DaneGry.ustawStage(new FXMLLoader(getClass().getResource("screen/wyborlvl.fxml")),600,400,"wyborlvl");
        DaneGry.poziomy.show();
    }
    public void pomoc() {
        DaneGry.menu.close();
        DaneGry.pomoc = DaneGry.ustawStage(new FXMLLoader(getClass().getResource("screen/pomoc.fxml")),600,400,"pomoc");
        DaneGry.pomoc.show();

    }
    public void wyjscie() {
        DaneGry.menu.close();
        System.exit(0);
    }


}
