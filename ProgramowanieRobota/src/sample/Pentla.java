package sample;

public class Pentla {

    int zapamietanaLinia;
    boolean wykonac;
    int licznik;
    String a;
    String b;
    String warunek;

    public void setWykonac(boolean wykonac) {
        this.wykonac = wykonac;
    }



    public Pentla(int zapamietanaLinia, int licznik) {
        this.warunek = warunek;
        this.licznik = licznik;
        this.zapamietanaLinia = zapamietanaLinia;
        this.a = "1";
        this.b = "1";
        this.warunek = "==";
        this.wykonac = true;
    }


    public Pentla(int zapamietanaLinia, String a, String warunek, String b) {
        this.warunek = warunek;
        this.zapamietanaLinia = zapamietanaLinia;
        this.a = a;
        this.b = b;
        this.warunek = warunek;
        this.licznik = 0;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }


    public int getLicznik() {
        return licznik;
    }

    public void setLicznik(int licznik) {
        this.licznik = licznik;
    }

    public void aktualizuj(PamiecZmiennych pam) {

        String pomA = pam.ciagZPodmienionymiZmiennymi(getA());
        String pomB = pam.ciagZPodmienionymiZmiennymi(getB());
        int a = Integer.parseInt(pomA);
        int b = Integer.parseInt(pomB);





        if (warunek.equals("==")) {
            if (a != b) {
                setWykonac(false);
            } else {
                setWykonac(true);
            }
        } else if (warunek.equals("<=")) {
            if (a > b) {
                setWykonac(false);
            } else {
                setWykonac(true);
            }
        } else if (warunek.equals(">=")) {
            if (a < b) {
                setWykonac(false);
            } else {
                setWykonac(true);
            }
        } else if (warunek.equals(">")) {
            if (a <= b) {
                setWykonac(false);
            } else {
                setWykonac(true);
            }
        } else if (warunek.equals("<")) {
            if (a >= b) {
                setWykonac(false);
            } else {
                setWykonac(true);
            }
        } else {
            if (a == b) {
                setWykonac(false);
            } else {
                setWykonac(true);
            }
        }


    }

    public void p() {
        System.out.println("////////");
        System.out.println("licznik " + licznik);
        System.out.println("wykonac " + wykonac);
        System.out.println("zapamietanaLinia " + zapamietanaLinia);
        System.out.println("a " + a);
        System.out.println("b " + b);
        System.out.println("warunek " + warunek);
    }



    public int getZapamietanaLinia() {
        return zapamietanaLinia;
    }

    public void setZapamietanaLinia(int zapamietanaLinia) {
        this.zapamietanaLinia = zapamietanaLinia;
    }

    public boolean czyWykonac() {
        return wykonac;
    }

}


